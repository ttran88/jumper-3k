using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class LavaStart : MonoBehaviour
{
    // Start is called before the first frame update
    public float lavaTopSpeed = 1;
    public float lavaBottomSpeed = 1;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.GetChild(0).position = transform.GetChild(0).position + (Vector3.up * lavaTopSpeed) * Time.deltaTime;
        transform.GetChild(1).localScale = transform.GetChild(1).localScale + (Vector3.up * lavaBottomSpeed) * Time.deltaTime;
    }
}
