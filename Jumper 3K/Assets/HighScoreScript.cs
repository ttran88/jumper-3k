using UnityEngine;
using UnityEngine.UI;

public class HighScoreScript : MonoBehaviour
{
    // Start is called before the first frame update
    public Text sbScoreText;
    public Text sbHighScoreText;
    public PlayerMover playerMover;

    public Text scoreTimer;
    public float score;
    public float highScore;

    void Start()
    {
        playerMover = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMover>();

        if (PlayerPrefs.HasKey("Highscore"))
        {
            highScore = PlayerPrefs.GetFloat("Highscore");
        }
        else
        {
            PlayerPrefs.SetFloat("Highscore", 300);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SaveHighScore()
    {
        score = float.Parse(scoreTimer.text);

        if (score < PlayerPrefs.GetFloat("Highscore"))
        {
            PlayerPrefs.SetFloat("Highscore", score);
            PlayerPrefs.Save();
            sbHighScoreText.text = "New Best Time!";
            sbScoreText.text = score.ToString();
        }
        else
        {
            sbHighScoreText.text = "Best Time: " + highScore.ToString();
            sbScoreText.text = "Your Time: " + score.ToString();
        }
    }
}
