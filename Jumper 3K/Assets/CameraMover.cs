using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject player;
    public float cameraOffSet;
    public float playerOffSet;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float playerLocation = GameObject.FindWithTag("Player").transform.position.y + playerOffSet;
        if ( playerLocation > -2.5)
        {
            transform.position = new Vector3(transform.position.x, player.transform.position.y + cameraOffSet, transform.position.z);
        }
        else
        {
            transform.position = new Vector3(0, 0, -10);
        }
    }
}
