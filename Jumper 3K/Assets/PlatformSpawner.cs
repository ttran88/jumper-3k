using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformSpawner : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject platform;
    public float spawnRate = 2;
    private float timer = 0;
    void Start()
    {
        SpawnPlatform();
    }

    // Update is called once per frame
    void Update()
    {
        if (timer < spawnRate) 
        {
            timer += Time.deltaTime;
        }
        else
        {
            SpawnPlatform();
            timer = 0;
        }
    }

    void SpawnPlatform()
    {
        Instantiate(platform);
    }
}
