using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMover : MonoBehaviour
{
    // Start is called before the first frame update

    public LogicScript logic;
    public LavaTrigger lavaTrigger;
    public HighScoreScript hsScript;

    public float speed;
    public float move;
    public float jump;
    public bool isJumping;
    public bool playerIsAlive = true;

    private Rigidbody2D rb;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        logic = GameObject.FindGameObjectWithTag("Logic").GetComponent<LogicScript>();
        lavaTrigger = GameObject.FindGameObjectWithTag("LavaTrigger").GetComponent<LavaTrigger>();
        hsScript = GameObject.FindGameObjectWithTag("hs").GetComponent<HighScoreScript>();
    }

    // Update is called once per frame
    void Update()
    {

        if (playerIsAlive == true)
        {
            move = Input.GetAxis("Horizontal");
            rb.velocity = new Vector2(speed * move, rb.velocity.y);

            if (Input.GetButtonDown("Jump") && isJumping == false)
            {
                rb.AddForce(new Vector2(rb.velocity.x, jump));
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Ground"))
        {
            isJumping = false;
        }
        if(collision.gameObject.CompareTag("Platform"))
        {
            isJumping = false;
        }
        if (collision.gameObject.CompareTag("Lava"))
        {
            logic.gameLost();
            playerIsAlive = false;
            lavaTrigger.enabled = false;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Ground"))
        {
            isJumping = true;
        }
        if(collision.gameObject.CompareTag("Platform"))
        {
            isJumping = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Portal"))
        {
            hsScript.SaveHighScore();
            logic.gameWin();
            playerIsAlive = false;
            lavaTrigger.enabled = false;
        }
    }
}
