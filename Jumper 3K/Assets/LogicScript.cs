using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LogicScript : MonoBehaviour
{
    public GameObject gameWinScreen;
    public GameObject gameLostScreen;

    public void restartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void gameWin()
    {
        gameWinScreen.SetActive(true);
    }

    public void gameLost()
    {
        gameLostScreen.SetActive(true);
    }

    public void gameStart()
    {
        SceneManager.LoadScene("GameStart");
    }

    public void gameQuit()
    {
        Application.Quit();
    }

}
