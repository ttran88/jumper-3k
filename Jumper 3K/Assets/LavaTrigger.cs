using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

public class LavaTrigger : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject lava;
    public Text timer;

    public bool isTrigger;

    private float timeToDisplay;
    private float seconds;
    private float minutes;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isTrigger)
        {
            StartTimer();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        lava.SetActive(true);
        isTrigger = true;
    }

    private void StartTimer()
    {
        timeToDisplay += Time.deltaTime;
        seconds = Mathf.FloorToInt(timeToDisplay % 60);
        minutes = Mathf.FloorToInt(timeToDisplay / 60);

        if (timeToDisplay > 60)
        {
            timer.text = string.Format("{0:0}:{1:00}", minutes, seconds);
        }
        else
        {
            if (timeToDisplay > 9)
            {
                timer.text = string.Format("{00}", seconds);
            }
            else
            {
                timer.text = string.Format("{0}", seconds);
            }
        }
    }



}
